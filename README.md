Godot FNoise by Alexis S.  
GDNative C++ module that brings to Godot the FastNoiseLite library by Jordan Peck.  

Uses NativeScript 1.1 - Only compatible with Godot 3.1 and higher.

Everything required to compile this module is provided here :
https://docs.godotengine.org/en/stable/tutorials/plugins/gdnative/gdnative-cpp-example.html

tldr :
run the follwing command in the root directory
`scons platform=<target-os>`

The module binary will be located in the bin/ directory with the .gdnlib and .gdns files.