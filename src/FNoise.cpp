#include "FNoise.h"

void godot::FNoise::_register_methods()
{
    register_method("set_seed", &godot::FNoise::set_seed);
    register_method("set_frequency", &godot::FNoise::set_frequency);
    register_method("set_noise_type", &godot::FNoise::set_noise_type);
    register_method("set_rotation_type_3d", &godot::FNoise::set_rotation_type_3d);
    register_method("set_fractal_type", &godot::FNoise::set_fractal_type);
    register_method("set_fractal_octaves", &godot::FNoise::set_fractal_octaves);
    register_method("set_fractal_lacunarity", &godot::FNoise::set_fractal_lacunarity);
    register_method("set_fractal_gain", &godot::FNoise::set_fractal_gain);
    register_method("set_fractal_weighted_strength", &godot::FNoise::set_fractal_weighted_strength);
    register_method("set_fractal_pingpong_strength", &godot::FNoise::set_fractal_pingpong_strength);
    register_method("set_cellular_distance_function", &godot::FNoise::set_cellular_distance_function);
    register_method("set_cellular_return_type", &godot::FNoise::set_cellular_return_type);
    register_method("set_cellular_jitter", &godot::FNoise::set_cellular_jitter);
    register_method("set_domain_warp_type", &godot::FNoise::set_domain_warp_type);
    register_method("set_domain_warp_amp", &godot::FNoise::set_domain_warp_amp);
    register_method("get_noise_2d", &godot::FNoise::get_noise_2d);
    register_method("get_noise_3d", &godot::FNoise::get_noise_3d);
    register_method("domain_warp_2d", &godot::FNoise::domain_warp_2d);
    register_method("domain_warp_3d", &godot::FNoise::domain_warp_3d);
}

void godot::FNoise::_init() { }

godot::FNoise::FNoise() { }

godot::FNoise::~FNoise() { }

void godot::FNoise::set_seed(int seed)
{
    _noise.SetSeed(seed);
}

void godot::FNoise::set_frequency(float frequency)
{
    _noise.SetFrequency(frequency);
}

void godot::FNoise::set_noise_type(int noise_type)
{
    switch (noise_type)
    {
        case 0:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2);
            break;
        case 1:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_OpenSimplex2S);
            break;
        case 2:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_Cellular);
            break;
        case 3:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_Perlin);
            break;
        case 4:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_ValueCubic);
            break;
        case 5:
            _noise.SetNoiseType(FastNoiseLite::NoiseType_Value);
            break;
    }
}

void godot::FNoise::set_rotation_type_3d(int rotation_type)
{
    switch (rotation_type)
    {
        case 0:
            _noise.SetRotationType3D(FastNoiseLite::RotationType3D_None);
            break;
        case 1:
            _noise.SetRotationType3D(FastNoiseLite::RotationType3D_ImproveXYPlanes);
            break;
        case 2:
            _noise.SetRotationType3D(FastNoiseLite::RotationType3D_ImproveXZPlanes);
            break;
    }
}

void godot::FNoise::set_fractal_type(int fractal_type)
{
    switch (fractal_type)
    {
        case 0:
            _noise.SetFractalType(FastNoiseLite::FractalType_None);
            break;
        case 1:
            _noise.SetFractalType(FastNoiseLite::FractalType_FBm);
            break;
        case 2:
            _noise.SetFractalType(FastNoiseLite::FractalType_Ridged);
            break;
        case 3:
            _noise.SetFractalType(FastNoiseLite::FractalType_PingPong);
            break;
        case 4:
            _noise.SetFractalType(FastNoiseLite::FractalType_DomainWarpProgressive);
            break;
        case 5:
            _noise.SetFractalType(FastNoiseLite::FractalType_DomainWarpIndependent);
            break;
    }
}

void godot::FNoise::set_fractal_octaves(int octaves)
{
    _noise.SetFractalOctaves(octaves);
}

void godot::FNoise::set_fractal_lacunarity(float lacunarity)
{
    _noise.SetFractalLacunarity(lacunarity);
}

void godot::FNoise::set_fractal_gain(float gain)
{
    _noise.SetFractalGain(gain);
}

void godot::FNoise::set_fractal_weighted_strength(float weighted_strength)
{
    _noise.SetFractalWeightedStrength(weighted_strength);
}

void godot::FNoise::set_fractal_pingpong_strength(float pingpong_strength)
{
    _noise.SetFractalWeightedStrength(pingpong_strength);
}

void godot::FNoise::set_cellular_distance_function(int cellular_distance_function)
{
    switch (cellular_distance_function)
    {
        case 0:
            _noise.SetCellularDistanceFunction(FastNoiseLite::CellularDistanceFunction_Euclidean);
            break;
        case 1:
            _noise.SetCellularDistanceFunction(FastNoiseLite::CellularDistanceFunction_EuclideanSq);
            break;
        case 2:
            _noise.SetCellularDistanceFunction(FastNoiseLite::CellularDistanceFunction_Manhattan);
            break;
        case 3:
            _noise.SetCellularDistanceFunction(FastNoiseLite::CellularDistanceFunction_Hybrid);
            break;
    }
}

void godot::FNoise::set_cellular_return_type(int cellular_return_type)
{
    switch (cellular_return_type)
    {
        case 0:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_CellValue);
            break;
        case 1:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance);
            break;
        case 2:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance2);
            break;
        case 3:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance2Add);
            break;
        case 4:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance2Sub);
            break;
        case 5:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance2Mul);
            break;
        case 6:
            _noise.SetCellularReturnType(FastNoiseLite::CellularReturnType_Distance2Div);
            break;
    }
}

void godot::FNoise::set_cellular_jitter(float cellular_jitter)
{
    _noise.SetCellularJitter(cellular_jitter);
}

void godot::FNoise::set_domain_warp_type(int domain_warp_type)
{
    switch (domain_warp_type)
    {
        case 0:
            _noise.SetDomainWarpType(FastNoiseLite::DomainWarpType_OpenSimplex2);
            break;
        case 1:
            _noise.SetDomainWarpType(FastNoiseLite::DomainWarpType_OpenSimplex2Reduced);
            break;
        case 2:
            _noise.SetDomainWarpType(FastNoiseLite::DomainWarpType_BasicGrid);
            break;
    }
}

void godot::FNoise::set_domain_warp_amp(float domain_warp_amp)
{
    _noise.SetDomainWarpAmp(domain_warp_amp);
}

float godot::FNoise::get_noise_2d(float x, float y)
{
    return _noise.GetNoise(x, y);
}

float godot::FNoise::get_noise_3d(float x, float y, float z)
{
    return _noise.GetNoise(x, y, z);
}

void godot::FNoise::domain_warp_2d(float x, float y)
{
    _noise.DomainWarp(x, y);
}

void godot::FNoise::domain_warp_3d(float x, float y, float z)
{
    _noise.DomainWarp(x, y, z);
}