#ifndef FNOISE_H
#define FNOISE_H

#include <Godot.hpp>
#include <Reference.hpp>
#include "FastNoiseLite.h"

namespace godot
{
    class FNoise : public Reference
    {
        GODOT_CLASS(FNoise, Reference);

    public:
        static void _register_methods();
        void _init();

        FNoise();
        ~FNoise();

        void set_seed(int);
        void set_frequency(float);
        void set_noise_type(int);
        void set_rotation_type_3d(int);
        void set_fractal_type(int);
        void set_fractal_octaves(int);
        void set_fractal_lacunarity(float);
        void set_fractal_gain(float);
        void set_fractal_weighted_strength(float);
        void set_fractal_pingpong_strength(float);
        void set_cellular_distance_function(int);
        void set_cellular_return_type(int);
        void set_cellular_jitter(float);
        void set_domain_warp_type(int);
        void set_domain_warp_amp(float);

        float get_noise_2d(float x, float y);
        float get_noise_3d(float x, float y, float z);
        
        void domain_warp_2d(float x, float y);
        void domain_warp_3d(float x, float y, float z);


    private:
        FastNoiseLite _noise;
    };
}

#endif